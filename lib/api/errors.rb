# frozen_string_literal: true

module API
  class Error < StandardError; end
  class NotFoundError < StandardError; end
end
