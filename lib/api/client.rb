# frozen_string_literal: true

module API
  class Client
    def self.index
      new.entries
    end

    def self.show(id)
      new.entry(id)
    end

    def entries
      safely_use_api do
        contentful.entries(content_type: 'recipe')
      end
    end

    def entry(id)
      safely_use_api do
        entry = contentful.entry(id)

        raise(NotFoundError, 'Not found entry') unless entry

        entry
      end
    end

    private

    def contentful
      @client ||= Contentful::Client.new(
        space: Settings.config.space,
        access_token: Settings.config.access_token,
        entry_mapping: { 'recipe' => Recipe },
        environment: Settings.config.env
      )
    end

    def safely_use_api(&block)
      yield
    rescue Contentful::NotFound
      raise API::NotFoundError
    rescue Contentful::Unauthorized, Contentful::BadRequest
      raise API::Error
    end
  end
end
