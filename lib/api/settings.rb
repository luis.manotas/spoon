# frozen_string_literal: true

require 'dry-configurable'

module API
  class Settings
    extend Dry::Configurable

    setting :access_token
    setting :content_type
    setting :mapping_class
    setting :asset_class
    setting :env
    setting :locale, 'en-US'
    setting :space
  end
end
