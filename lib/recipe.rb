# frozen_string_literal: true

class Recipe < Contentful::Entry
  def self.list
    API::Client.index
  end

  def self.get(id)
    API::Client.show(id)
  end

  def chef_name
    return unless fields.has_key?(:chef)

    chef.name
  end

  def full_photo
    photo_url(quality: 100)
  end

  def photo_preview
    photo_url(height: 550, width: 550, quality: 50)
  end

  def tags_by_comma
    return unless fields.has_key?(:tags)

    tags.map(&:name).join(', ')
  end

  private

  def photo_url(specifications = {})
    photo.url(specifications)
  end
end
