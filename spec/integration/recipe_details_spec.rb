# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Show recipe details' do
  describe 'WHEN accesing to recipe' do
    specify 'THEN see the description, chef, tags' do
      mock_contentful_api_entry

      get 'recipe/437eO3ORCME46i02SeCW46'

      page = last_response.body
      expect(page).to include('Crispy Chicken and Rice')
      expect(page).to include('Crispy chicken skin, tender meat, and rich')
      expect(page).to include('Jony Chives')
      expect(page).to include('gluten free')
    end
  end

  describe 'WHEN visiting an unexisting recipe' do
    specify 'THEN see the 404 page' do
      mock_contentful_api_not_found

      get 'recipe/non-existing'
  
      expect(last_response.body).to include('Oops')
    end
  end
end
