# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Show recipe details' do
  describe 'WHEN recipe does not exist' do
    specify 'THEN render Opps page' do
      get '/non-existing/page'

      page = last_response.body
      expect(page).to include('Oops')
      expect(page).to include('You are looking in the wrong place')
    end
  end
end
