# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'List recipes' do
  describe 'WHEN accesing to /' do
    specify 'THEN see the list of recipes' do
      mock_contentful_api_entry_list

      get '/'

      page = last_response.body
      expect(page).to include('Recipes')
      expect(page).to include('Tofu Saag Paneer')
    end
  end
end
