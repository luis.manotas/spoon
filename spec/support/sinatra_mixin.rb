# frozen_string_literal: true

module SinatraMixin
  include Rack::Test::Methods
  def app() Sinatra::Application end
end
