# frozen_string_literal: true

module Helper
  def mock_contentful_api_not_found
    response = load_fixtue_file('contentful/recipe_not_found.json')

    stub_request(:get, "https://cdn.contentful.com/spaces/kk2bw5ojx476/environments/master/entries?sys.id=non-existing")
      .to_return(status: 200, body: response)
  end

  def mock_contentful_api_entry
    response = load_fixtue_file('contentful/recipe.json')

    stub_request(:get, "https://cdn.contentful.com/spaces/kk2bw5ojx476/environments/master/entries?sys.id=437eO3ORCME46i02SeCW46")
      .to_return(status: 200, body: response)
  end

  def mock_contentful_api_entry_list
    response = load_fixtue_file('contentful/recipe_list.json')

    stub_request(:get, "https://cdn.contentful.com/spaces/kk2bw5ojx476/environments/master/entries?content_type=recipe")
      .to_return(status: 200, body: response)
  end

  def mock_contentful_entry_no_tag
    response = load_fixtue_file('contentful/recipe_no_tags.json')

    stub_request(:get, "https://cdn.contentful.com/spaces/kk2bw5ojx476/environments/master/entries?sys.id=2E8bc3VcJmA8OgmQsageas")
      .to_return(status: 200, body: response)
  end

  def mock_contentful_entry_no_chef
    response = load_fixtue_file('contentful/recipe_no_chef.json')

    stub_request(:get, "https://cdn.contentful.com/spaces/kk2bw5ojx476/environments/master/entries?sys.id=5jy9hcMeEgQ4maKGqIOYW6")
      .to_return(status: 200, body: response)
  end

  def mock_contentful_error(code)
    response = load_fixtue_file('contentful/error.json')

    stub_request(:get, /https:\/\/cdn.contentful.com\/spaces/)
      .to_return(status: code, body: response)
  end

  def load_fixtue_file(filename)
    File.new(File.join('.', 'spec', 'fixtures', filename))
  end
end
