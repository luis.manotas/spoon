# frozen_string_literal: true

require 'spec_helper'

RSpec.shared_examples 'Handle contentful API errors' do
  context 'Contentful API errors' do
    context 'WHEN not found space' do
      specify 'THEN raise API::NotFoundError' do
        mock_contentful_error(404)

        expect { described_class.index }.to raise_error(API::NotFoundError)
      end
    end

    context 'WHEN not authorized' do
      specify 'THEN raise API::Error' do
        mock_contentful_error(401)

        expect { described_class.index }.to raise_error(API::Error)
      end
    end

    context 'WHEN bad request' do
      specify 'THEN raise API::Error' do
        mock_contentful_error(400)

        expect { described_class.index }.to raise_error(API::Error)
      end
    end
  end
end

RSpec.describe API::Client do
  describe 'GIVEN #index' do
    before { mock_contentful_api_entry_list }

    specify 'THEN use Contentful::Client to fetch the list' do
      expect_any_instance_of(Contentful::Client).to receive(:entries).with(content_type: 'recipe').and_call_original

      described_class.index
    end

    specify 'THEN return the list' do
      output = described_class.index

      expect(output.is_a?(Contentful::Array)).to eq(true)
    end

    context 'WHEN error from Contentful API' do
      let(:action) { described_class.index }

      it_behaves_like 'Handle contentful API errors'
    end
  end

  describe 'GIVEN #show(id)' do
    let(:existing) { '437eO3ORCME46i02SeCW46' }

    before { mock_contentful_api_entry }

    specify 'THEN use Contentful::Client to fetch recipe' do
      expect_any_instance_of(Contentful::Client).to receive(:entry).with(existing).and_call_original

      described_class.show(existing)
    end

    specify 'THEN return the recipe' do
      output = described_class.show(existing)

      expect(output.is_a?(Contentful::Entry)).to eq(true)
    end

    context 'WHEN Recipe is NOT FOUND (nil returned by API)' do
      specify 'THEN raise API::Client::NotFoundError when non existing recipe' do
        mock_contentful_api_not_found
        expect { described_class.show('non-existing') }.to raise_error(API::NotFoundError)
      end
    end

    context 'WHEN error from Contentful API' do
      let(:action) { described_class.show('ID') }

      it_behaves_like 'Handle contentful API errors'
    end
  end
end
