# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Recipe do
  describe '#list' do
    specify 'THEN use API::Client.index method' do
      expect(API::Client).to receive(:index)

      described_class.list
    end
  end

  describe '#get' do
    specify 'THEN use API::Client.show(id) method' do
      expect(API::Client).to receive(:show).with(1)

      described_class.get(1)
    end
  end

  describe 'data fields' do
    let(:with_full_info) { API::Client.show('437eO3ORCME46i02SeCW46') }
    let(:with_no_tag) { API::Client.show('2E8bc3VcJmA8OgmQsageas') }
    let(:with_no_chef) { API::Client.show('5jy9hcMeEgQ4maKGqIOYW6') }

    before do
      mock_contentful_api_entry
      mock_contentful_entry_no_tag
      mock_contentful_entry_no_chef
    end

    describe '#chef_name' do
      context 'GIVEN recipe with chef' do
        specify 'THEN respond with chef name' do
          expect(with_full_info.chef_name).to eq('Jony Chives')
        end
      end

      context 'GIVEN recipe without chef' do
        specify 'THEN respond with nil' do
          expect(with_no_chef.chef_name).to be_nil
        end
      end
    end

    describe '#tags_by_comma' do
      context 'GIVEN recipe with tags' do
        specify 'THEN respond with tags separated by comma' do
          expect(with_full_info.tags_by_comma).to eq('gluten free, healthy')
        end
      end

      context 'GIVEN recipe without tags' do
        specify 'THEN respond with nil' do
          expect(with_no_tag.tags_by_comma).to be_nil
        end
      end
    end
  end
end
