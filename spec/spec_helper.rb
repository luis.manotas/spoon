# frozen_string_literal: true

require 'rack/test'
require 'rspec'
require 'support/helper'
require 'support/sinatra_mixin'
require 'webmock/rspec'

ENV['RACK_ENV'] = 'test'

require File.expand_path '../../app.rb', __FILE__

RSpec.configure do |config|
  config.include SinatraMixin
  config.include Helper

  config.before(:all) do
    WebMock.disable_net_connect!(allow_localhost: true)
  end
end
