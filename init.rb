# frozen_string_literal: true

# Loading env vars
require 'dotenv'
Dotenv.load
# Required libraries
require 'contentful'
require 'sinatra'
require './lib/recipe'
require './lib/api/errors'
require './lib/api/settings'
require './lib/api/client'

# Setting contentful credentials
API::Settings.config.space = ENV.fetch('CONTENTFUL_SPACE')
API::Settings.config.access_token = ENV.fetch('CONTENTFUL_ACCESS_TOKEN')
API::Settings.config.env = ENV.fetch('CONTENTFUL_ENVIRONMENT')
