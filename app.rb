# frozen_string_literal: true

require './init'

get '/' do
  @recipes = Recipe.list

  erb :index
end

get '/recipe/:id' do
  @recipe = Recipe.get(params[:id])

  erb :show
end

error API::NotFoundError, API::Error do
  erb :oops
end

not_found do
  erb :oops
end
