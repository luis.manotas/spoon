# Spoon

Shows a list of recipes fetched from Contenful.
Basically the app is just two pages:

* Index or list of recipes
* Show or detail from recipe

# Usage

Installing dependencies.

```
$ bundle install
````

Running application

````
$ bundle exec rackup
`````

# Tests

Project is using `Rspec`

```
$ bundle exec rspec
```

Also by running

```
$ bundle exec rake
````

# Requirements

* Ruby 2.6.4
